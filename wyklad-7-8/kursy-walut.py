# Ubuntu/Debian: sudo apt install python3-tk

# pip install PyMsgBox
import pymsgbox

# pip install python-dateutil
import dateutil.parser

# pip install requests
import requests


APP_TITLE = "Kursy walut"
NBP_JSON_URL_PATTERN = "http://api.nbp.pl/api/exchangerates/rates/A/{currency}/{date}?format=json"


def ask_currency():
    currency = pymsgbox.prompt(text="Podaj walute:",
                               title=APP_TITLE)
    return currency


def ask_date():
    date = pymsgbox.prompt(text="Podaj date waluty:",
                           title=APP_TITLE)
    return date


def show_rate(currency, rate, date):
    pymsgbox.alert(text=f"Kurs sredni: 1 {currency.upper()} = {rate} PLN w dniu {date.day}.{date.month}.{date.year}",
                   title=APP_TITLE)

    
def show_alert(text):
    pymsgbox.alert(text=text, title=APP_TITLE)
    

def main():
    try:
        currency = ask_currency()
        if not currency:
            return

        date = ask_date()
        if not date:
            return

        parsed_date = dateutil.parser.parse(date, dayfirst=True)
        date_for_url = parsed_date.strftime("%Y-%m-%d")

        # wyslac pytanie do nbp
        url = NBP_JSON_URL_PATTERN.format(currency=currency,
                                          date=date_for_url)

        # wyswietl odpowiedz
        response = requests.get(url)
        if response.ok:
            data = response.json()
            rate = data['rates'][0]['mid']

            show_rate(currency, rate, parsed_date)
        else:
            show_alert("Brak danych!")
    except Exception as e:
        show_alert("Brak danych!")
    
if __name__ == '__main__':
    main()
    
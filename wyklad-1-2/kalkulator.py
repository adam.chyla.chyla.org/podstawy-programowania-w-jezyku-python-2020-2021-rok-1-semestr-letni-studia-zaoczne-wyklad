xs = input('Podaj liczbe x: ')
ys = input('Podaj liczbe y: ')

op = input('Wybierz operacje (+,-,*,/): ')

bledna_liczba = False

# sprawdzamy poprawnosc xs
for znak in xs:
    # jesli znak z napisu xs znajduje sie w '-012345689'
    if znak in '-0123456789':
        pass
    else:
        print('Bledny znak:', znak)
        bledna_liczba = True

# sprawdzamy poprawnosc ys
for znak in ys:
    # jesli znak z napisu ys znajduje sie w '-012345689'
    if znak in '-0123456789':
        pass
    else:
        print('Bledny znak:', znak)
        bledna_liczba = True


if bledna_liczba is True:
    print('Nie wykonuje obliczen')
else:
    x = float(xs)
    y = float(ys)

    if op == '+':
        print('Wynik:', x + y)
    elif op == '-':
        print('Wynik:', x - y)
    elif op == '*':
        print('Wynik:', x * y)
    elif op == '/':
        if ys == '0':
            print('Blad dzielenia przez 0!')
        else:
            print('Wynik:', x / y)
    else:
        print('Bledna operacja.')

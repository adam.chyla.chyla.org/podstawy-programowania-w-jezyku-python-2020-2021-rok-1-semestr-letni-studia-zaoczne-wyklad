#
# TDD ---> Test Driven Development
#
# 1. Piszemy test (assert)
# 2. Uruchamiamy testy (widzimy, ze nie dzialaja)
# 3. Piszemy implmementacje (kod funkcji)
# 4. Uruchamiamy testy (widzimy, ze dzilaja)
# 5. Refactor (zmiana kodu, ale bez wprowadzania dodatkowej funkcjonalnosci)
# 6. Wracamy do 1.
def czy_liczba(tekst):
    czy_wystepuje_cyfra = False
    for znak in tekst:
        if znak in "1234567890":
            czy_wystepuje_cyfra = True

    if tekst != "" and tekst[0] in "-1234567890":
        for znak in tekst[1:]:
            if znak not in "1234567890":
                return False

    return czy_wystepuje_cyfra


def dodaj(*args):
    suma = 0

    for x in args:
        suma += x

    return suma

from utils import czy_liczba, dodaj

# Testy
# - sprawdamy czy funkcja dziala jak zakladalismy
# - zabezpieczamy nasz kod przed nieprawidowym dzialaniem (wprowadzeniem bledu)
# - dokumentacja - przyklad uzycia, przyklad dzialania


def test_napis_pusty_nie_jest_liczba():
    assert czy_liczba("") is False  # True/False/None is zamiast ==


def test_jeden_jest_liczba():
    assert czy_liczba("1") is True


def test_a_nie_jest_liczba():
    assert czy_liczba("a") is False


def test_123_jest_liczba():
    assert czy_liczba("123") is True


def test_napis_z_litera_na_koncu_nie_jest_liczba():
    assert czy_liczba("1a") is False


def test_napis_z_literami_w_srodku_nie_jest_liczba():
    assert czy_liczba("123abc123") is False


def test_minus_nie_jest_liczba():
    assert czy_liczba("-") is False


def test_liczba_ujemna_jest_liczba():
    assert czy_liczba("-1") is True


def test_litery_z_minusem_na_poczatku_nie_sa_liczba():
    assert czy_liczba("-abc") is False


def test_plus_nie_jest_liczba():
    assert czy_liczba("+") is False


def test_napis_z_plusem_w_srodku_nie_jest_liczba():
    assert czy_liczba("123+12") is False


def test_napis_z_minusem_w_srodku_nie_jest_liczba():
    assert czy_liczba("12-1234") is False


def test_dodaj_funkcja_jest_wywolana_bez_parametrow_wtedy_zwraca_zero():
    assert dodaj() == 0


def test_dodaj_funkcja_jest_wywolywana_z_jednym_argumentem_wtedy_zwraca_ten_argument():
    assert dodaj(2) == 2


def test_dodaj_funkcja_jest_wywolywana_z_dwoma_argumentami_wtedy_zwraca_sume_argumentow():
    assert dodaj(1, 2) == 3


def test_dodaj_funkcja_jest_wywolywana_z_wieloma_argumentami_wtedy_zwraca_sume_argumentow():
    assert dodaj(1, 2, 3, 4) == 10

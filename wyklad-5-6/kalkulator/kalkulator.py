# from utils import czy_liczba, dodaj
import utils

def zapisz_do_historii(tekst):
    with open('historia', 'a') as f:
        f.write(tekst + '\n')  # napis

        
def wyswietl_historie():
    with open('historia') as f:
        for i, linia in enumerate(f, start=1):
            print("{}. {}".format(i, linia.strip()))


def main():
    while True:
        op = input('Wybierz operacje (+,-,*,/,K,H): ')

        if op == 'K':
            break
        elif op == 'H':
            wyswietl_historie()
            continue
            
        xs = input('Podaj liczbe x: ')
        ys = input('Podaj liczbe y: ')

        # if czy_liczba(xs) and czy_liczba(ys):
        if utils.czy_liczba(xs) and utils.czy_liczba(ys):
            x = float(xs)
            y = float(ys)

            if op == '+':
                napis = "{} + {} = {}".format(xs, ys, utils.dodaj(x,y))
                zapisz_do_historii(napis)
                print(napis)  # napis\n
            elif op == '-':
                napis = f"{xs} - {ys} = {x - y}"  # f-string
                zapisz_do_historii(napis)
                print(napis)
            elif op == '*':
                napis = "%s * %s = %f" % (xs, ys, x * y)
                zapisz_do_historii(napis)
                print(napis)
            elif op == '/':
                print(f"{x=}") # x=WARTOSC
                if ys == '0':
                    print('Blad dzielenia przez 0!')
                    zapisz_do_historii(f'{x} / {y} --> BLAD')
                else:
                    napis = f"{xs} / {ys} = {x / y}"  # f-string
                    zapisz_do_historii(napis)
                    print(napis)
            else:
                zapisz_do_historii(f"Bledna operacja: {op}")
                print('Bledna operacja.')
        else:
            print('Nie wykonuje obliczen')


if __name__ == "__main__":
    main()

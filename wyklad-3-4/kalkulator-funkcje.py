#
# TDD ---> Test Driven Development
#
# 1. Piszemy test (assert)
# 2. Uruchamiamy testy (widzimy, ze nie dzialaja)
# 3. Piszemy implmementacje (kod funkcji)
# 4. Uruchamiamy testy (widzimy, ze dzilaja)
# 5. Refactor (zmiana kodu, ale bez wprowadzania dodatkowej funkcjonalnosci)
# 6. Wracamy do 1.
def czy_liczba(tekst):
    czy_wystepuje_cyfra = False
    for znak in tekst:
        if znak in "1234567890":
            czy_wystepuje_cyfra = True

    if tekst != "" and tekst[0] in "-1234567890":
        for znak in tekst[1:]:
            if znak not in "1234567890":
                return False

    return czy_wystepuje_cyfra


# Testy
# - sprawdamy czy funkcja dziala jak zakladalismy
# - zabezpieczamy nasz kod przed nieprawidowym dzialaniem (wprowadzeniem bledu)
# - dokumentacja - przyklad uzycia, przyklad dzialania
assert czy_liczba("") is False # True/False/None is zamiast ==
assert czy_liczba("1") is True, "1 powinno byc liczba"
assert czy_liczba("a") is False
assert czy_liczba("123") is True
assert czy_liczba("1a") is False
assert czy_liczba("123abc123") is False
assert czy_liczba("-") is False
assert czy_liczba("-1") is True
assert czy_liczba("-abc") is False
assert czy_liczba("+") is False
assert czy_liczba("123+12") is False
assert czy_liczba("12-1234") is False


def dodaj(*args):
    suma = 0
    
    for x in args:
        suma += x

    return suma

assert dodaj() == 0
assert dodaj(2) == 2
assert dodaj(1, 2) == 3
assert dodaj(1, 2, 3, 4) == 10


def main():
    xs = input('Podaj liczbe x: ')
    ys = input('Podaj liczbe y: ')

    op = input('Wybierz operacje (+,-,*,/): ')


    if czy_liczba(xs) and czy_liczba(ys):
        x = float(xs)
        y = float(ys)

        if op == '+':
            print('Wynik:', dodaj(x, y))
        elif op == '-':
            print('Wynik:', x - y)
        elif op == '*':
            print('Wynik:', x * y)
        elif op == '/':
            if ys == '0':
                print('Blad dzielenia przez 0!')
            else:
                print('Wynik:', x / y)
        else:
            print('Bledna operacja.')
    else:
        print('Nie wykonuje obliczen')

main()

#from utils import czy_liczba, dodaj
import utils


def main():
    xs = input('Podaj liczbe x: ')
    ys = input('Podaj liczbe y: ')

    op = input('Wybierz operacje (+,-,*,/): ')


    #if czy_liczba(xs) and czy_liczba(ys):
    if utils.czy_liczba(xs) and utils.czy_liczba(ys):
        x = float(xs)
        y = float(ys)

        if op == '+':
            print('Wynik:', utils.dodaj(x, y))
        elif op == '-':
            print('Wynik:', x - y)
        elif op == '*':
            print('Wynik:', x * y)
        elif op == '/':
            if ys == '0':
                print('Blad dzielenia przez 0!')
            else:
                print('Wynik:', x / y)
        else:
            print('Bledna operacja.')
    else:
        print('Nie wykonuje obliczen')


if __name__ == "__main__":
    main()
